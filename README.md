# gozilla client

## About

CLI to manage gozilla

To enable debug logs, set GOZ_DEBUG=1 in env

Some options can be set via env variables:

* GOZ_USER to match --user
* GOZ_APIKEY to match --apikey
* GOZ_URL to match --url

## Examples

    # Upload
    go run goz.go gozFileHandler.go --user test --apikey 123 --url http://localhost:8080 version upload --from README.md osallou/perso/pack1/v1.0/README.md

    # Publish
    go run goz.go gozFileHandler.go --user test --apikey 123 --url http://localhost:8080 version publish osallou/perso/pack1/v1.0

    # Download
    go run goz.go gozFileHandler.go --user test --apikey 123 --url http://localhost:8080 version download --to /tmp/README.md osallou/perso/pack1/v1.0/README.md

    # View file info
    go run goz.go gozFileHandler.go --user test --apikey 123 --url http://localhost:8080 version info osallou/perso/pack1/v1.0/README.md
