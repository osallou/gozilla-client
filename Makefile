VERSION := $(shell git rev-parse --short HEAD)
BUILDTIME := $(shell date -u '+%Y-%m-%dT%H:%M:%SZ')

GOLDFLAGS += -X main.Version=$(VERSION)
GOFLAGS = -ldflags "$(GOLDFLAGS)"

run: build

build:
	GOOS=linux GOARCH=amd64 go build -o gozilla-client.linux.amd64 $(GOFLAGS) . ; \
        GOOS=darwin GOARCH=amd64 go build -o gozilla-client.darwin.amd64 $(GOFLAGS) . ; \
        GOOS=windows GOARCH=amd64 go build  -o gozilla-client.windows.amd64.exe $(GOFLAGS) .


