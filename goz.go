package main

import (
	"bytes"
	b64 "encoding/base64"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
	"time"

	humanize "github.com/dustin/go-humanize"
	"github.com/rs/zerolog"

	goz "gitlab.inria.fr/osallou/gozilla-lib"

	"github.com/olekukonko/tablewriter"
)

// Version is client version
var Version string

// OptionsDef command line options
type OptionsDef struct {
	UID    string
	APIKEY string
	URL    string
	Token  string
}

// ShowUsage display base options
func ShowUsage(options []string) {
	fmt.Println("Usage:")
	fmt.Println("  Expected environment variables:")
	fmt.Println("    * GOZ_USER: user identifier")
	fmt.Println("    * GOZ_APIKEY: user api key")
	fmt.Println("    * GOZ_URL: URL to gozilla server")
	for _, option := range options {
		fmt.Printf("  goz %s -h\n", option)
	}
}

func getVisibility(v goz.VisibilityType) string {
	switch v {
	case goz.VisibilityPublic:
		return "public"
	case goz.VisibilityProtected:
		return "protected"
	case goz.VisibilityPrivate:
		return "private"
	}
	return "unknown"
}

func getProfile(v goz.Profile) string {
	switch v {
	case goz.ProfileAdmin:
		return "admin"
	case goz.ProfileRepoAdmin:
		return "repo admin"
	case goz.ProfileDev:
		return "developper"
	case goz.ProfileGuest:
		return "guest"
	}
	return "unknown"
}

func getUserProfile(p goz.UserProfile) string {
	switch p {
	case goz.UserProfileAdmin:
		return "admin"
	case goz.UserProfileOperator:
		return "operator"
	case goz.UserProfileUser:
		return "user"
	}
	return "unknown"
}

func handleSubject(options OptionsDef, args []string) error {
	var err error

	switch args[0] {
	case "create":
		cmdOptions := flag.NewFlagSet("create options", flag.ExitOnError)
		subjectID := cmdOptions.String("id", "", "subject unique id")
		subjectDesc := cmdOptions.String("description", "", "subject description")
		subjectVisibility := cmdOptions.String("visibility", "public", "visibility [public|protected|private]")
		subjectType := cmdOptions.String("type", "perso", "type [perso|org]")

		cmdOptions.Parse(args[1:])

		remainingArgs := cmdOptions.Args()
		if len(remainingArgs) == 1 {
			elts := strings.Split(remainingArgs[0], "/")
			if len(elts) == 1 {
				subjectID = &elts[0]
			}
		}

		if *subjectID == "" {
			return fmt.Errorf("Missing subject identifier")
		}

		subject := goz.Subject{
			ID:          *subjectID,
			Description: *subjectDesc,
		}
		switch *subjectVisibility {
		case "public":
			subject.Visibility = goz.VisibilityPublic
			break
		case "protected":
			subject.Visibility = goz.VisibilityProtected
			break
		case "private":
			subject.Visibility = goz.VisibilityPrivate
			break
		default:
			subject.Visibility = goz.VisibilityPublic
		}

		switch *subjectType {
		case "perso":
			subject.Type = goz.SubjectPersonal
			break
		case "org":
			subject.Type = goz.SubjectOrg
			break
		default:
			subject.Type = goz.SubjectPersonal
		}

		jsonData, _ := json.Marshal(subject)
		client := &http.Client{}
		req, _ := http.NewRequest("POST", fmt.Sprintf("%s/api/v1.0/%s", options.URL, "repos"), bytes.NewBuffer(jsonData))
		if options.APIKEY != "" {
			req.Header.Set("X-GOZ-USER", options.UID)
			req.Header.Set("X-GOZ-APIKEY", options.APIKEY)
		}
		resp, _ := client.Do(req)
		if err != nil {
			return err
		}
		defer resp.Body.Close()
		if resp.StatusCode != 201 {
			fmt.Printf("Invalid response code: %d\n", resp.StatusCode)
			body, err := ioutil.ReadAll(resp.Body)
			if err == nil {
				fmt.Printf("Error: %s", body)
			}
			return fmt.Errorf("Create command failed")
		}
		fmt.Printf("Subject created\n")
		break
	case "update":
		return fmt.Errorf("not implemented")
	case "list":
		client := &http.Client{}
		req, _ := http.NewRequest("GET", fmt.Sprintf("%s/api/v1.0/%s", options.URL, "repos"), nil)
		if options.APIKEY != "" {
			req.Header.Set("X-GOZ-USER", options.UID)
			req.Header.Set("X-GOZ-APIKEY", options.APIKEY)
		}
		resp, _ := client.Do(req)
		if err != nil {
			return err
		}
		defer resp.Body.Close()
		if resp.StatusCode != 200 {
			fmt.Printf("Invalid response code: %d\n", resp.StatusCode)
			body, err := ioutil.ReadAll(resp.Body)
			if err == nil {
				fmt.Printf("Error: %s", body)
			}
			return fmt.Errorf("List command failed")
		}
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return err
		}

		type SubjectResult struct {
			Subjects []goz.Subject `json:"subjects"`
		}
		var subjectsResult SubjectResult
		err = json.Unmarshal(body, &subjectsResult)
		if err != nil {
			return err
		}
		fmt.Println("Subjects:")
		for _, subject := range subjectsResult.Subjects {
			if subject.Description == "" {
				subject.Description = "--"
			}
			fmt.Printf("* %s: %s [%s]\n", subject.ID, subject.Description, subject.Owner)
		}

		break
	case "show":
		if len(args) < 2 {
			return fmt.Errorf("subject id missing: goz subject show XX")
		}
		id := args[1]
		client := &http.Client{}
		req, _ := http.NewRequest("GET", fmt.Sprintf("%s/api/v1.0/%s/%s", options.URL, "repos", id), nil)
		if options.APIKEY != "" {
			req.Header.Set("X-GOZ-USER", options.UID)
			req.Header.Set("X-GOZ-APIKEY", options.APIKEY)
		}
		resp, _ := client.Do(req)
		if err != nil {
			return err
		}
		defer resp.Body.Close()
		if resp.StatusCode != 200 {
			fmt.Printf("Invalid response code: %d\n", resp.StatusCode)
			body, err := ioutil.ReadAll(resp.Body)
			if err == nil {
				fmt.Printf("Error: %s", body)
			}
			return fmt.Errorf("List command failed")
		}
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return err
		}

		type SubjectResult struct {
			Subject goz.Subject `json:"subject"`
			Repos   []goz.Repo
		}
		var subjectResult SubjectResult
		err = json.Unmarshal(body, &subjectResult)
		if err != nil {
			return err
		}
		fmt.Printf("Name: %s\n", subjectResult.Subject.ID)
		fmt.Printf("Owner: %s\n", subjectResult.Subject.Owner)
		fmt.Printf("Visibility: %s\n", getVisibility(subjectResult.Subject.Visibility))
		fmt.Printf("Customer: %s\n", subjectResult.Subject.Customer)
		fmt.Println("Repos:")
		for _, repo := range subjectResult.Repos {
			fmt.Printf("* %s: %s\n", repo.ID, repo.Description)
		}
		break
	case "delete":
		cmdOptions := flag.NewFlagSet("delete options", flag.ExitOnError)
		packageSubject := cmdOptions.String("subject", "", "subject id")
		packageForce := cmdOptions.Bool("force", false, "force removal")
		cmdOptions.Parse(args[1:])

		remainingArgs := cmdOptions.Args()
		if len(remainingArgs) == 1 {
			elts := strings.Split(remainingArgs[0], "/")
			if len(elts) == 1 {
				// subject/repo
				packageSubject = &elts[0]
			}
		}

		if *packageSubject == "" {
			return fmt.Errorf("Missing subject identifier")
		}
		if !promptConfirm("Confirm subject deletion?") {
			return fmt.Errorf("cancelled")
		}
		return deleteSubject(options, *packageSubject, *packageForce)
	}
	return err
}

func handleRepo(options OptionsDef, args []string) error {
	var err error

	switch args[0] {
	case "create":
		cmdOptions := flag.NewFlagSet("create options", flag.ExitOnError)
		repoID := cmdOptions.String("id", "", "repo unique id")
		repoSubject := cmdOptions.String("subject", "", "subject id")
		repoDesc := cmdOptions.String("description", "", "subject description")
		repoHome := cmdOptions.String("home", "", "home page url")
		repoIssues := cmdOptions.String("issues", "", "isssue page url")
		repoVisibility := cmdOptions.String("visibility", "public", "visibility [public|protected|private]")
		cmdOptions.Parse(args[1:])

		remainingArgs := cmdOptions.Args()
		if len(remainingArgs) == 1 {
			elts := strings.Split(remainingArgs[0], "/")
			if len(elts) == 2 {
				// subject/repo
				repoSubject = &elts[0]
				repoID = &elts[1]

			}
		}

		if *repoID == "" {
			return fmt.Errorf("Missing identifier")

		}
		if *repoSubject == "" {
			return fmt.Errorf("Missing subject")

		}
		repo := goz.Repo{
			ID:          *repoID,
			Subject:     *repoSubject,
			Description: *repoDesc,
			HomePage:    *repoHome,
			IssuesPage:  *repoIssues,
		}
		switch *repoVisibility {
		case "public":
			repo.Visibility = goz.VisibilityPublic
			break
		case "protected":
			repo.Visibility = goz.VisibilityProtected
			break
		case "private":
			repo.Visibility = goz.VisibilityPrivate
			break
		default:
			repo.Visibility = goz.VisibilityPublic
		}
		jsonData, _ := json.Marshal(repo)
		client := &http.Client{}
		req, _ := http.NewRequest("POST", fmt.Sprintf("%s/api/v1.0/%s/%s", options.URL, "repos", *repoSubject), bytes.NewBuffer(jsonData))
		if options.APIKEY != "" {
			req.Header.Set("X-GOZ-USER", options.UID)
			req.Header.Set("X-GOZ-APIKEY", options.APIKEY)
		}
		resp, _ := client.Do(req)
		if err != nil {
			return err
		}
		defer resp.Body.Close()
		if resp.StatusCode != 201 {
			fmt.Printf("Invalid response code: %d\n", resp.StatusCode)
			body, err := ioutil.ReadAll(resp.Body)
			if err == nil {
				fmt.Printf("Error: %s", body)
			}
			return fmt.Errorf("Create command failed")
		}
		fmt.Printf("Repo created\n")
		break
	case "update":
		return fmt.Errorf("not implemented")
	case "list":
		cmdOptions := flag.NewFlagSet("list options", flag.ExitOnError)
		repoSubject := cmdOptions.String("subject", "", "subject id")
		cmdOptions.Parse(args[1:])

		remainingArgs := cmdOptions.Args()
		if len(remainingArgs) == 1 {
			elts := strings.Split(remainingArgs[0], "/")
			if len(elts) == 1 {
				// subject/repo
				repoSubject = &elts[0]
			}
		}

		client := &http.Client{}
		req, _ := http.NewRequest("GET", fmt.Sprintf("%s/api/v1.0/%s/%s", options.URL, "repos", *repoSubject), nil)
		if options.APIKEY != "" {
			req.Header.Set("X-GOZ-USER", options.UID)
			req.Header.Set("X-GOZ-APIKEY", options.APIKEY)
		}
		resp, _ := client.Do(req)
		if err != nil {
			return err
		}
		defer resp.Body.Close()
		if resp.StatusCode != 200 {
			fmt.Printf("Invalid response code: %d\n", resp.StatusCode)
			body, err := ioutil.ReadAll(resp.Body)
			if err == nil {
				fmt.Printf("Error: %s", body)
			}
			return fmt.Errorf("List command failed")
		}
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return err
		}

		type RepoResult struct {
			Subject goz.Subject `json:"subject"`
			Repos   []goz.Repo  `json:"repos"`
		}
		var reposResult RepoResult
		err = json.Unmarshal(body, &reposResult)
		if err != nil {
			return err
		}
		fmt.Printf("Subject: %s\n", reposResult.Subject.ID)
		fmt.Println("Repos:")
		for _, repo := range reposResult.Repos {
			if repo.Description == "" {
				repo.Description = "--"
			}
			fmt.Printf("* %s: %s\n", repo.ID, repo.Description)
		}

		break
	case "show":
		cmdOptions := flag.NewFlagSet("show options", flag.ExitOnError)
		repoSubject := cmdOptions.String("subject", "", "subject id")
		repoID := cmdOptions.String("repo", "", "repo id")

		cmdOptions.Parse(args[1:])

		remainingArgs := cmdOptions.Args()
		if len(remainingArgs) == 1 {
			elts := strings.Split(remainingArgs[0], "/")
			if len(elts) == 2 {
				// subject/repo
				repoSubject = &elts[0]
				repoID = &elts[1]
			}
		}

		if *repoID == "" || *repoSubject == "" {
			return fmt.Errorf("Missing subject or repo identifier")
		}

		client := &http.Client{}
		req, _ := http.NewRequest("GET", fmt.Sprintf("%s/api/v1.0/%s/%s/%s", options.URL, "repos", *repoSubject, *repoID), nil)
		if options.APIKEY != "" {
			req.Header.Set("X-GOZ-USER", options.UID)
			req.Header.Set("X-GOZ-APIKEY", options.APIKEY)
		}
		resp, _ := client.Do(req)
		if err != nil {
			return err
		}
		defer resp.Body.Close()
		if resp.StatusCode != 200 {
			fmt.Printf("Invalid response code: %d\n", resp.StatusCode)
			body, err := ioutil.ReadAll(resp.Body)
			if err == nil {
				fmt.Printf("Error: %s", body)
			}
			return fmt.Errorf("Show command failed")
		}
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return err
		}

		type RepoResult struct {
			Subject string   `json:"subject"`
			Repo    goz.Repo `json:"repo"`
		}
		var reposResult RepoResult
		err = json.Unmarshal(body, &reposResult)
		if err != nil {
			return err
		}
		fmt.Printf("Subject: %s\n", reposResult.Subject)
		fmt.Printf("Repo: %s\n", reposResult.Repo.ID)
		fmt.Printf("Description: %s\n", reposResult.Repo.Description)
		fmt.Printf("Visibility: %s\n", getVisibility(reposResult.Repo.Visibility))
		fmt.Println("Members:")
		for _, member := range reposResult.Repo.Members {
			fmt.Printf("%s: %s\n", member.User, getProfile(member.Profile))
		}

		break
	case "delete":
		cmdOptions := flag.NewFlagSet("list options", flag.ExitOnError)
		packageSubject := cmdOptions.String("subject", "", "subject id")
		packageRepo := cmdOptions.String("repo", "", "repo id")
		packageForce := cmdOptions.Bool("force", false, "force removal")
		cmdOptions.Parse(args[1:])

		remainingArgs := cmdOptions.Args()
		if len(remainingArgs) == 1 {
			elts := strings.Split(remainingArgs[0], "/")
			if len(elts) == 2 {
				// subject/repo
				packageSubject = &elts[0]
				packageRepo = &elts[1]
			}
		}

		if *packageSubject == "" || *packageRepo == "" {
			return fmt.Errorf("Missing subject identifier")
		}
		if !promptConfirm("Confirm repo deletion?") {
			return fmt.Errorf("cancelled")
		}
		return deleteRepo(options, *packageSubject, *packageRepo, *packageForce)
	}
	return err
}

func handlePackage(options OptionsDef, args []string) error {

	var err error

	switch args[0] {
	case "create":
		cmdOptions := flag.NewFlagSet("create options", flag.ExitOnError)
		packageID := cmdOptions.String("id", "", "package unique id in repo")
		packageSubject := cmdOptions.String("subject", "", "subject id")
		packageRepo := cmdOptions.String("repo", "", "repo id")
		packageType := cmdOptions.String("type of package [raw|debian|rpm]", "raw", "package type")

		cmdOptions.Parse(args[1:])

		remainingArgs := cmdOptions.Args()
		if len(remainingArgs) == 1 {
			elts := strings.Split(remainingArgs[0], "/")
			if len(elts) >= 3 {
				// subject/repo
				packageSubject = &elts[0]
				packageRepo = &elts[1]
				packageID = &elts[2]
			}
			if len(elts) == 4 {
				packageType = &elts[3]
			}
		}

		if *packageID == "" {
			return fmt.Errorf("Missing identifier")
		}
		if *packageSubject == "" {
			return fmt.Errorf("Missing subject")
		}
		if *packageRepo == "" {
			return fmt.Errorf("Missing repo")
		}

		packType := goz.RawPackage
		switch *packageType {
		case "raw":
			packType = goz.RawPackage
			break
		case "debian":
			packType = goz.DebianPackage
			break
		case "rpm":
			packType = goz.RpmPackage
			break

		}

		pack := goz.Package{
			ID:      *packageID,
			Subject: *packageSubject,
			Repo:    *packageRepo,
			Type:    packType,
		}

		jsonData, _ := json.Marshal(pack)
		client := &http.Client{}
		req, _ := http.NewRequest("POST", fmt.Sprintf("%s/api/v1.0/%s/%s/%s", options.URL, "packages", *packageSubject, *packageRepo), bytes.NewBuffer(jsonData))
		if options.APIKEY != "" {
			req.Header.Set("X-GOZ-USER", options.UID)
			req.Header.Set("X-GOZ-APIKEY", options.APIKEY)
		}
		resp, _ := client.Do(req)
		if err != nil {
			return err
		}
		defer resp.Body.Close()
		if resp.StatusCode != 201 {
			fmt.Printf("Invalid response code: %d\n", resp.StatusCode)
			body, err := ioutil.ReadAll(resp.Body)
			if err == nil {
				fmt.Printf("Error: %s", body)
			}
			return fmt.Errorf("Create command failed")
		}
		fmt.Printf("Package created\n")
		break
	case "update":
		return fmt.Errorf("not implemented")
	case "list":
		cmdOptions := flag.NewFlagSet("list options", flag.ExitOnError)
		packageSubject := cmdOptions.String("subject", "", "subject id")
		packageRepo := cmdOptions.String("repo", "", "repo id")
		cmdOptions.Parse(args[1:])

		remainingArgs := cmdOptions.Args()
		if len(remainingArgs) == 1 {
			elts := strings.Split(remainingArgs[0], "/")
			if len(elts) == 2 {
				// subject/repo
				packageSubject = &elts[0]
				packageRepo = &elts[1]
			}
		}

		if *packageSubject == "" || *packageRepo == "" {
			return fmt.Errorf("Missing subject or repo identifier")
		}

		client := &http.Client{}
		req, _ := http.NewRequest("GET", fmt.Sprintf("%s/api/v1.0/%s/%s/%s", options.URL, "packages", *packageSubject, *packageRepo), nil)
		if options.APIKEY != "" {
			req.Header.Set("X-GOZ-USER", options.UID)
			req.Header.Set("X-GOZ-APIKEY", options.APIKEY)
		}
		resp, _ := client.Do(req)
		if err != nil {
			return err
		}
		defer resp.Body.Close()
		if resp.StatusCode != 200 {
			fmt.Printf("Invalid response code: %d\n", resp.StatusCode)
			body, err := ioutil.ReadAll(resp.Body)
			if err == nil {
				fmt.Printf("Error: %s", body)
			}
			return fmt.Errorf("List command failed")
		}
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return err
		}

		type PackageListResult struct {
			Subject  string        `json:"subject"`
			Repo     string        `json:"repo"`
			Packages []goz.Package `json:"packages"`
		}
		var packsResult PackageListResult
		err = json.Unmarshal(body, &packsResult)
		if err != nil {
			return err
		}
		fmt.Printf("Subject: %s\n", packsResult.Subject)
		fmt.Printf("Repo: %s\n", packsResult.Repo)
		fmt.Println("Packages:")
		for _, pack := range packsResult.Packages {
			fmt.Printf("* %s: %+v\n", pack.ID, pack.Meta)
			for _, v := range pack.Versions {
				fmt.Printf("  * %s\n", v)
			}
		}
		break
	case "show":
		cmdOptions := flag.NewFlagSet("show options", flag.ExitOnError)
		packageSubject := cmdOptions.String("subject", "", "subject id")
		packageRepo := cmdOptions.String("repo", "", "repo id")
		packageID := cmdOptions.String("id", "", "package id")

		cmdOptions.Parse(args[1:])

		remainingArgs := cmdOptions.Args()
		if len(remainingArgs) == 1 {
			elts := strings.Split(remainingArgs[0], "/")
			if len(elts) == 3 {
				// subject/repo
				packageSubject = &elts[0]
				packageRepo = &elts[1]
				packageID = &elts[2]
			}
		}

		if *packageSubject == "" || *packageRepo == "" || *packageID == "" {
			return fmt.Errorf("Missing subject,repo or package identifier")
		}

		client := &http.Client{}
		req, _ := http.NewRequest("GET", fmt.Sprintf("%s/api/v1.0/%s/%s/%s/%s", options.URL, "packages", *packageSubject, *packageRepo, *packageID), nil)
		if options.APIKEY != "" {
			req.Header.Set("X-GOZ-USER", options.UID)
			req.Header.Set("X-GOZ-APIKEY", options.APIKEY)
		}
		resp, _ := client.Do(req)
		if err != nil {
			return err
		}
		defer resp.Body.Close()
		if resp.StatusCode != 200 {
			fmt.Printf("Invalid response code: %d\n", resp.StatusCode)
			body, err := ioutil.ReadAll(resp.Body)
			if err == nil {
				fmt.Printf("Error: %s", body)
			}
			return fmt.Errorf("List command failed")
		}
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return err
		}

		type PackageShowResult struct {
			Subject string      `json:"subject"`
			Repo    string      `json:"repo"`
			Package goz.Package `json:"package"`
		}
		var packsResult PackageShowResult
		err = json.Unmarshal(body, &packsResult)
		if err != nil {
			return err
		}
		fmt.Printf("Subject: %s\n", packsResult.Subject)
		fmt.Printf("Repo: %s\n", packsResult.Repo)
		fmt.Printf("Package: %s\n", packsResult.Package.ID)
		fmt.Printf("Meta: %+v\n", packsResult.Package.Meta)
		fmt.Printf("Created: %s\n", time.Unix(packsResult.Package.Created, 0))
		break
	case "delete":
		cmdOptions := flag.NewFlagSet("list options", flag.ExitOnError)
		packageSubject := cmdOptions.String("subject", "", "subject id")
		packageRepo := cmdOptions.String("repo", "", "repo id")
		packageID := cmdOptions.String("id", "", "package id")
		packageForce := cmdOptions.Bool("force", false, "force removal")
		cmdOptions.Parse(args[1:])

		remainingArgs := cmdOptions.Args()
		if len(remainingArgs) == 1 {
			elts := strings.Split(remainingArgs[0], "/")
			if len(elts) == 3 {
				// subject/repo
				packageSubject = &elts[0]
				packageRepo = &elts[1]
				packageID = &elts[2]
			}
		}

		if *packageSubject == "" || *packageRepo == "" || *packageID == "" {
			return fmt.Errorf("Missing subject, repo or package identifier")
		}

		if !promptConfirm("Confirm package deletion?") {
			return fmt.Errorf("cancelled")
		}
		return deletePackage(options, *packageSubject, *packageRepo, *packageID, *packageForce)

	}
	return err
}

// GozConfig TODO
type GozConfig struct {
	Version    string
	APIVersion string
	Storage    goz.StorageType
}

func getConfig(options OptionsDef) (*GozConfig, error) {
	client := &http.Client{}
	req, _ := http.NewRequest("GET", fmt.Sprintf("%s/api", options.URL), nil)
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	if resp.StatusCode != 200 {
		fmt.Printf("Invalid response code: %d\n", resp.StatusCode)
		body, err := ioutil.ReadAll(resp.Body)
		if err == nil {
			fmt.Printf("Error: %s", body)
		}
		return nil, fmt.Errorf("List command failed")
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	var cfg map[string]interface{}
	err = json.Unmarshal(body, &cfg)

	gozCfg := GozConfig{
		APIVersion: cfg["apiVersion"].(string),
		Version:    cfg["version"].(string),
		Storage:    goz.StorageType(cfg["storage"].(float64)),
	}
	return &gozCfg, err
}

func handleMe(options OptionsDef) error {
	client := &http.Client{}
	req, _ := http.NewRequest("GET", fmt.Sprintf("%s/api/v1.0/user", options.URL), nil)
	if options.APIKEY != "" {
		creds := fmt.Sprintf("%s:%s", options.UID, options.APIKEY)
		b64Creds := b64.StdEncoding.EncodeToString([]byte(creds))
		req.Header.Set("Authorization", fmt.Sprintf("Basic %s", b64Creds))
	}
	resp, err := client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	if resp.StatusCode != 200 {
		fmt.Printf("Invalid response code: %d\n", resp.StatusCode)
		body, err := ioutil.ReadAll(resp.Body)
		if err == nil {
			fmt.Printf("Error: %s", body)
		}
		return fmt.Errorf("Create command failed")
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	type MeMessageResult struct {
		User goz.User     `json:"user"`
		Plan goz.PlanInfo `json:"plan"`
	}
	var meResult MeMessageResult
	err = json.Unmarshal(body, &meResult)
	if err != nil {
		return err
	}
	table := tablewriter.NewWriter(os.Stdout)
	table.SetHeader([]string{"ID", "Email", "APIKey", "Customer", "Profile"})
	table.Append([]string{meResult.User.ID, meResult.User.Email, meResult.User.APIKey, meResult.User.Customer, getUserProfile(meResult.User.Profile)})
	table.Render()
	return nil

}

func handlePackageVersion(options OptionsDef, args []string) error {

	var err error

	switch args[0] {
	case "discard":
		cmdOptions := flag.NewFlagSet("discard options", flag.ExitOnError)
		packageID := cmdOptions.String("id", "", "package version unique id in package")
		packageSubject := cmdOptions.String("subject", "", "subject id")
		packageRepo := cmdOptions.String("repo", "", "repo id")
		packagePackage := cmdOptions.String("package", "", "package id in repo")
		packagePath := cmdOptions.String("path", "", "remote file path")

		cmdOptions.Parse(args[1:])

		remainingArgs := cmdOptions.Args()
		if len(remainingArgs) == 1 {
			elts := strings.Split(remainingArgs[0], "/")
			if len(elts) >= 4 {
				// subject/repo
				packageSubject = &elts[0]
				packageRepo = &elts[1]
				packagePackage = &elts[2]
				packageID = &elts[3]
			}
			if len(elts) > 4 {
				filePath := strings.Join(elts[4:], "/")
				packagePath = &filePath
			}
		}

		if *packageSubject == "" || *packageRepo == "" || *packagePackage == "" || *packageID == "" {
			return fmt.Errorf("Missing subject, repo, package or version identifier")
		}

		if *packagePath == "" {
			return fmt.Errorf("Missing file path")
		}

		gozCfg, gozErr := getConfig(options)
		if gozErr != nil {
			return fmt.Errorf("Failed to fetch configuration from remote server")
		}
		storeFileHandler := NewStorageFileHandler(Config{
			StorageType: gozCfg.Storage,
			GozServer:   options.URL,
			UID:         options.UID,
			APIKEY:      options.APIKEY,
			APIVersion:  gozCfg.APIVersion,

			Subject: *packageSubject,
			Repo:    *packageRepo,
			Package: *packagePackage,
			Version: *packageID,
		}, *packageSubject)

		if storeFileHandler == nil {
			return fmt.Errorf("Failed to init storage")
		}
		err := storeFileHandler.DeleteFile(*packagePath)
		return err

	case "delete":
		cmdOptions := flag.NewFlagSet("delete options", flag.ExitOnError)
		packageID := cmdOptions.String("id", "", "package version unique id in package")
		packageSubject := cmdOptions.String("subject", "", "subject id")
		packageRepo := cmdOptions.String("repo", "", "repo id")
		packagePackage := cmdOptions.String("package", "", "package id in repo")
		packageForce := cmdOptions.Bool("force", false, "delete all files with version")

		cmdOptions.Parse(args[1:])

		remainingArgs := cmdOptions.Args()
		if len(remainingArgs) == 1 {
			elts := strings.Split(remainingArgs[0], "/")
			if len(elts) == 4 {
				// subject/repo
				packageSubject = &elts[0]
				packageRepo = &elts[1]
				packagePackage = &elts[2]
				packageID = &elts[3]
			}
		}

		if *packageSubject == "" || *packageRepo == "" || *packagePackage == "" || *packageID == "" {
			return fmt.Errorf("Missing subject, repo, package or version identifier")
		}

		if !promptConfirm("Confirm package version deletion?") {
			return fmt.Errorf("cancelled")
		}
		return deleteVersion(options, *packageSubject, *packageRepo, *packagePackage, *packageID, *packageForce)
	case "info":
		cmdOptions := flag.NewFlagSet("info options", flag.ExitOnError)
		packageID := cmdOptions.String("id", "", "package version unique id in package")
		packageSubject := cmdOptions.String("subject", "", "subject id")
		packageRepo := cmdOptions.String("repo", "", "repo id")
		packagePackage := cmdOptions.String("package", "", "package id in repo")
		packageFrom := cmdOptions.String("from", "", "remote file path")

		cmdOptions.Parse(args[1:])

		remainingArgs := cmdOptions.Args()
		if len(remainingArgs) == 1 {
			elts := strings.Split(remainingArgs[0], "/")
			if len(elts) >= 4 {
				// subject/repo
				packageSubject = &elts[0]
				packageRepo = &elts[1]
				packagePackage = &elts[2]
				packageID = &elts[3]
			}
			if len(elts) > 4 {
				to := strings.Join(elts[4:], "/")
				packageFrom = &to
			}
		}

		if *packageSubject == "" || *packageRepo == "" || *packagePackage == "" || *packageID == "" {
			return fmt.Errorf("Missing subject, repo, package or version identifier")
		}

		if *packageFrom == "" {
			return fmt.Errorf("Missing file path")
		}

		gozCfg, gozErr := getConfig(options)
		if gozErr != nil {
			return fmt.Errorf("Failed to fetch configuration from remote server")
		}
		storeFileHandler := NewStorageFileHandler(Config{
			StorageType: gozCfg.Storage,
			GozServer:   options.URL,
			UID:         options.UID,
			APIKEY:      options.APIKEY,
			APIVersion:  gozCfg.APIVersion,
		}, *packageSubject)

		if storeFileHandler == nil {
			return fmt.Errorf("Failed to init storage")
		}

		from := fmt.Sprintf("%s/%s/%s/%s/%s", *packageSubject, *packageRepo, *packagePackage, *packageID, *packageFrom)
		info, infoErr := storeFileHandler.Show(from)
		if infoErr != nil {
			return infoErr
		}
		lastUpdated := time.Unix(info.LastUpdated, 0)
		fmt.Printf("%s\t%s\t%d\n", from, lastUpdated, info.Size)
		if info.MD5 != "" {
			fmt.Printf("MD5: %s", info.MD5)
		}
		return nil
	case "upload":
		cmdOptions := flag.NewFlagSet("upload options", flag.ExitOnError)
		packageID := cmdOptions.String("id", "", "package version unique id in package")
		packageSubject := cmdOptions.String("subject", "", "subject id")
		packageRepo := cmdOptions.String("repo", "", "repo id")
		packagePackage := cmdOptions.String("package", "", "package id in repo")
		packageFrom := cmdOptions.String("from", "", "local file path")
		packageTo := cmdOptions.String("to", "", "remote file path")

		cmdOptions.Parse(args[1:])

		remainingArgs := cmdOptions.Args()
		if len(remainingArgs) == 1 {
			elts := strings.Split(remainingArgs[0], "/")
			if len(elts) >= 4 {
				// subject/repo
				packageSubject = &elts[0]
				packageRepo = &elts[1]
				packagePackage = &elts[2]
				packageID = &elts[3]
			}
			if len(elts) > 4 {
				to := strings.Join(elts[4:], "/")
				packageTo = &to
				if *packageFrom == "" {
					packageFrom = &to
				}
			}
		}

		if *packageSubject == "" || *packageRepo == "" || *packagePackage == "" || *packageID == "" {
			return fmt.Errorf("Missing subject, repo, package or version identifier")
		}

		if *packageFrom == "" || *packageTo == "" {
			return fmt.Errorf("could not determine from or to locations")
		}

		gozCfg, gozErr := getConfig(options)
		if gozErr != nil {
			return fmt.Errorf("Failed to fetch configuration from remote server")
		}
		storeFileHandler := NewStorageFileHandler(Config{
			StorageType: gozCfg.Storage,
			GozServer:   options.URL,
			UID:         options.UID,
			APIKEY:      options.APIKEY,
			APIVersion:  gozCfg.APIVersion,
		}, *packageSubject)

		if storeFileHandler == nil {
			return fmt.Errorf("Failed to init storage")
		}

		to := fmt.Sprintf("%s/%s/%s/%s/%s", *packageSubject, *packageRepo, *packagePackage, *packageID, *packageTo)
		uploadErr := storeFileHandler.Upload(*packageFrom, to)
		return uploadErr
	case "download":
		cmdOptions := flag.NewFlagSet("upload options", flag.ExitOnError)
		packageID := cmdOptions.String("id", "", "package version unique id in package")
		packageSubject := cmdOptions.String("subject", "", "subject id")
		packageRepo := cmdOptions.String("repo", "", "repo id")
		packagePackage := cmdOptions.String("package", "", "package id in repo")
		packageFrom := cmdOptions.String("from", "", "remote file path")
		packageTo := cmdOptions.String("to", "", "local file path")

		cmdOptions.Parse(args[1:])

		remainingArgs := cmdOptions.Args()
		if len(remainingArgs) == 1 {
			elts := strings.Split(remainingArgs[0], "/")
			if len(elts) >= 4 {
				// subject/repo
				packageSubject = &elts[0]
				packageRepo = &elts[1]
				packagePackage = &elts[2]
				packageID = &elts[3]
			}
			if len(elts) > 4 {
				to := strings.Join(elts[4:], "/")
				packageFrom = &to
				if *packageTo == "" {
					packageTo = &to
				}
			}
		}

		if *packageSubject == "" || *packageRepo == "" || *packagePackage == "" || *packageID == "" {
			return fmt.Errorf("Missing subject, repo, package or version identifier")
		}

		gozCfg, gozErr := getConfig(options)
		if gozErr != nil {
			return fmt.Errorf("Failed to fetch configuration from remote server")
		}
		storeFileHandler := NewStorageFileHandler(Config{
			StorageType: gozCfg.Storage,
			GozServer:   options.URL,
			UID:         options.UID,
			APIKEY:      options.APIKEY,
			APIVersion:  gozCfg.APIVersion,
		}, *packageSubject)

		if storeFileHandler == nil {
			return fmt.Errorf("Failed to init storage")
		}

		from := fmt.Sprintf("%s/%s/%s/%s/%s", *packageSubject, *packageRepo, *packagePackage, *packageID, *packageFrom)
		downErr := storeFileHandler.Download(from, *packageTo)
		return downErr
	case "create":
		cmdOptions := flag.NewFlagSet("create options", flag.ExitOnError)
		packageID := cmdOptions.String("id", "", "package version unique id in package")
		packageSubject := cmdOptions.String("subject", "", "subject id")
		packageRepo := cmdOptions.String("repo", "", "repo id")
		packagePackage := cmdOptions.String("package", "", "package id in repo")
		cmdOptions.Parse(args[1:])

		remainingArgs := cmdOptions.Args()
		if len(remainingArgs) == 1 {
			elts := strings.Split(remainingArgs[0], "/")
			if len(elts) == 4 {
				// subject/repo
				packageSubject = &elts[0]
				packageRepo = &elts[1]
				packagePackage = &elts[2]
				packageID = &elts[3]
			}
		}

		if *packageID == "" {
			return fmt.Errorf("Missing identifier")
		}
		if *packageSubject == "" {
			return fmt.Errorf("Missing subject")
		}
		if *packageRepo == "" {
			return fmt.Errorf("Missing repo")
		}
		if *packagePackage == "" {
			return fmt.Errorf("Missing package")
		}
		pack := goz.PackageVersion{
			Version: *packageID,
			Subject: *packageSubject,
			Repo:    *packageRepo,
			Package: *packagePackage,
		}

		jsonData, _ := json.Marshal(pack)
		client := &http.Client{}
		req, _ := http.NewRequest("POST", fmt.Sprintf("%s/api/v1.0/%s/%s/%s/%s/versions", options.URL, "packages", *packageSubject, *packageRepo, *packagePackage), bytes.NewBuffer(jsonData))
		if options.APIKEY != "" {
			req.Header.Set("X-GOZ-USER", options.UID)
			req.Header.Set("X-GOZ-APIKEY", options.APIKEY)
		}
		resp, _ := client.Do(req)
		if err != nil {
			return err
		}
		defer resp.Body.Close()
		if resp.StatusCode != 201 {
			fmt.Printf("Invalid response code: %d\n", resp.StatusCode)
			body, err := ioutil.ReadAll(resp.Body)
			if err == nil {
				fmt.Printf("Error: %s", body)
			}
			return fmt.Errorf("Create command failed")
		}
		fmt.Printf("Package version created\n")
		break
	case "update":
		return fmt.Errorf("not implemented")
	case "unpublish":
		type PublishMsg struct {
			Discard bool `json:"discard"`
		}
		unpubMsg := PublishMsg{
			Discard: true,
		}
		cmdOptions := flag.NewFlagSet("unpublish options", flag.ExitOnError)
		packageID := cmdOptions.String("id", "", "package version unique id in package")
		packageSubject := cmdOptions.String("subject", "", "subject id")
		packageRepo := cmdOptions.String("repo", "", "repo id")
		packagePackage := cmdOptions.String("package", "", "package id in repo")
		cmdOptions.Parse(args[1:])

		remainingArgs := cmdOptions.Args()
		if len(remainingArgs) == 1 {
			elts := strings.Split(remainingArgs[0], "/")
			if len(elts) == 4 {
				// subject/repo
				packageSubject = &elts[0]
				packageRepo = &elts[1]
				packagePackage = &elts[2]
				packageID = &elts[3]
			}
		}

		if *packageID == "" {
			return fmt.Errorf("Missing identifier")
		}
		if *packageSubject == "" {
			return fmt.Errorf("Missing subject")
		}
		if *packageRepo == "" {
			return fmt.Errorf("Missing repo")
		}
		if *packagePackage == "" {
			return fmt.Errorf("Missing package")
		}

		jsonData, _ := json.Marshal(unpubMsg)
		client := &http.Client{}
		req, _ := http.NewRequest("POST", fmt.Sprintf("%s/api/v1.0/%s/%s/%s/%s/%s/publish", options.URL, "content", *packageSubject, *packageRepo, *packagePackage, *packageID), bytes.NewBuffer(jsonData))
		if options.APIKEY != "" {
			req.Header.Set("X-GOZ-USER", options.UID)
			req.Header.Set("X-GOZ-APIKEY", options.APIKEY)
		}
		resp, _ := client.Do(req)
		if err != nil {
			return err
		}
		defer resp.Body.Close()
		if resp.StatusCode != 200 {
			fmt.Printf("Invalid response code: %d\n", resp.StatusCode)
			body, err := ioutil.ReadAll(resp.Body)
			if err == nil {
				fmt.Printf("Error: %s", body)
			}
			return fmt.Errorf("List command failed")
		}

		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return err
		}

		type PublishMessageResult struct {
			Message string
			Package string
			Version string
		}
		var publishResult PublishMessageResult
		err = json.Unmarshal(body, &publishResult)
		if err != nil {
			return err
		}
		fmt.Printf(publishResult.Message)
		return nil
	case "publish":
		cmdOptions := flag.NewFlagSet("publish options", flag.ExitOnError)
		packageID := cmdOptions.String("id", "", "package version unique id in package")
		packageSubject := cmdOptions.String("subject", "", "subject id")
		packageRepo := cmdOptions.String("repo", "", "repo id")
		packagePackage := cmdOptions.String("package", "", "package id in repo")
		cmdOptions.Parse(args[1:])

		remainingArgs := cmdOptions.Args()
		if len(remainingArgs) == 1 {
			elts := strings.Split(remainingArgs[0], "/")
			if len(elts) == 4 {
				// subject/repo
				packageSubject = &elts[0]
				packageRepo = &elts[1]
				packagePackage = &elts[2]
				packageID = &elts[3]
			}
		}

		if *packageID == "" {
			return fmt.Errorf("Missing identifier")
		}
		if *packageSubject == "" {
			return fmt.Errorf("Missing subject")
		}
		if *packageRepo == "" {
			return fmt.Errorf("Missing repo")
		}
		if *packagePackage == "" {
			return fmt.Errorf("Missing package")
		}

		client := &http.Client{}
		req, _ := http.NewRequest("POST", fmt.Sprintf("%s/api/v1.0/%s/%s/%s/%s/%s/publish", options.URL, "content", *packageSubject, *packageRepo, *packagePackage, *packageID), nil)
		if options.APIKEY != "" {
			req.Header.Set("X-GOZ-USER", options.UID)
			req.Header.Set("X-GOZ-APIKEY", options.APIKEY)
		}
		resp, _ := client.Do(req)
		if err != nil {
			return err
		}
		defer resp.Body.Close()
		if resp.StatusCode != 200 {
			fmt.Printf("Invalid response code: %d\n", resp.StatusCode)
			body, err := ioutil.ReadAll(resp.Body)
			if err == nil {
				fmt.Printf("Error: %s", body)
			}
			return fmt.Errorf("List command failed")
		}

		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return err
		}

		type PublishMessageResult struct {
			Message string
			Package string
			Version string
		}
		var publishResult PublishMessageResult
		err = json.Unmarshal(body, &publishResult)
		if err != nil {
			return err
		}
		fmt.Printf(publishResult.Message)
		return nil

	case "list":
		cmdOptions := flag.NewFlagSet("list options", flag.ExitOnError)
		packageSubject := cmdOptions.String("subject", "", "subject id")
		packageRepo := cmdOptions.String("repo", "", "repo id")
		packagePackage := cmdOptions.String("package", "", "package id")
		cmdOptions.Parse(args[1:])

		remainingArgs := cmdOptions.Args()
		if len(remainingArgs) == 1 {
			elts := strings.Split(remainingArgs[0], "/")
			if len(elts) == 3 {
				// subject/repo
				packageSubject = &elts[0]
				packageRepo = &elts[1]
				packagePackage = &elts[2]
			}
		}

		if *packageSubject == "" || *packageRepo == "" || *packagePackage == "" {
			return fmt.Errorf("Missing subject, repo or package identifier")
		}

		client := &http.Client{}
		req, _ := http.NewRequest("GET", fmt.Sprintf("%s/api/v1.0/%s/%s/%s/%s/versions", options.URL, "packages", *packageSubject, *packageRepo, *packagePackage), nil)
		if options.APIKEY != "" {
			req.Header.Set("X-GOZ-USER", options.UID)
			req.Header.Set("X-GOZ-APIKEY", options.APIKEY)
		}
		resp, _ := client.Do(req)
		if err != nil {
			return err
		}
		defer resp.Body.Close()
		if resp.StatusCode != 200 {
			fmt.Printf("Invalid response code: %d\n", resp.StatusCode)
			body, err := ioutil.ReadAll(resp.Body)
			if err == nil {
				fmt.Printf("Error: %s", body)
			}
			return fmt.Errorf("List command failed")
		}
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return err
		}

		type PackageListResult struct {
			Subject  string               `json:"subject"`
			Repo     string               `json:"repo"`
			Package  goz.Package          `json:"package"`
			Versions []goz.PackageVersion `json:"versions"`
		}
		var packsResult PackageListResult
		err = json.Unmarshal(body, &packsResult)
		if err != nil {
			return err
		}
		fmt.Printf("Subject: %s\n", packsResult.Subject)
		fmt.Printf("Repo: %s\n", packsResult.Repo)
		fmt.Printf("Package: %s\n", packsResult.Package.ID)
		fmt.Println("Versions:")
		for _, pack := range packsResult.Versions {
			fmt.Printf("* %s [published: %t]: %+v\n", pack.Version, pack.Published, pack.Meta)
		}
		break
	case "show":
		cmdOptions := flag.NewFlagSet("list options", flag.ExitOnError)
		packageSubject := cmdOptions.String("subject", "", "subject id")
		packageRepo := cmdOptions.String("repo", "", "repo id")
		packagePackage := cmdOptions.String("package", "", "package id")
		packageID := cmdOptions.String("id", "", "package version id")

		cmdOptions.Parse(args[1:])

		remainingArgs := cmdOptions.Args()
		if len(remainingArgs) == 1 {
			elts := strings.Split(remainingArgs[0], "/")
			if len(elts) == 4 {
				// subject/repo
				packageSubject = &elts[0]
				packageRepo = &elts[1]
				packagePackage = &elts[2]
				packageID = &elts[3]
			}
		}

		if *packageSubject == "" || *packageRepo == "" || *packagePackage == "" || *packageID == "" {
			return fmt.Errorf("Missing subject, repo, package or version identifier")
		}

		client := &http.Client{}
		req, _ := http.NewRequest("GET", fmt.Sprintf("%s/api/v1.0/%s/%s/%s/%s/versions/%s", options.URL, "packages", *packageSubject, *packageRepo, *packagePackage, *packageID), nil)
		if options.APIKEY != "" {
			req.Header.Set("X-GOZ-USER", options.UID)
			req.Header.Set("X-GOZ-APIKEY", options.APIKEY)
		}
		resp, _ := client.Do(req)
		if err != nil {
			return err
		}

		defer resp.Body.Close()
		if resp.StatusCode != 200 {
			fmt.Printf("Invalid response code: %d\n", resp.StatusCode)
			body, err := ioutil.ReadAll(resp.Body)
			if err == nil {
				fmt.Printf("Error: %s", body)
			}
			return fmt.Errorf("Show command failed")
		}
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return err
		}

		type PackageShowResult struct {
			Subject string              `json:"subject"`
			Repo    string              `json:"repo"`
			Package goz.Package         `json:"package"`
			Version goz.PackageVersion  `json:"version"`
			Files   []goz.RawFileObject `json:"files"`
		}
		var packsResult PackageShowResult
		err = json.Unmarshal(body, &packsResult)
		if err != nil {
			return err
		}

		fmt.Printf("Subject: %s\n", packsResult.Subject)
		fmt.Printf("Repo: %s\n", packsResult.Repo)
		fmt.Printf("Package: %s\n", packsResult.Package.ID)
		fmt.Printf("Version: %s\n", packsResult.Version.Version)
		fmt.Printf("Published: %t\n", packsResult.Version.Published)
		fmt.Printf("Meta: %+v\n", packsResult.Version.Meta)
		fmt.Printf("Created: %s\n", time.Unix(packsResult.Version.Created, 0))
		table := tablewriter.NewWriter(os.Stdout)
		table.SetHeader([]string{"Path", "Last updated", "Size"})

		for _, f := range packsResult.Files {
			name := f.GetName()
			extras := f.GetMeta()
			table.Append([]string{name, time.Unix(extras.LastUpdated, 0).String(), humanize.Bytes(uint64(extras.Size))})
		}
		table.Render()
		break
	}
	return err
}

func cliUsage() {
	flag.PrintDefaults()
	fmt.Printf("Env:\n")
	fmt.Printf(" * GOZ_NONINTERACTIVE: set to 1 to skip interactive prompts")
	fmt.Printf("Subcommands:\n")
	fmt.Printf(" * subject\n")
	fmt.Printf(" * repo\n")
	fmt.Printf(" * package\n")
	fmt.Printf(" * version\n")
	fmt.Printf(" * me\n")
}

func subjectUsage() {
	fmt.Println("Subject subject commands:")
	fmt.Println(" * list: list user subjects")
	fmt.Println(" * show ID: show subject ID in details")
	fmt.Println(" * edit ID: update subject ID info, see -h")
	fmt.Println(" * create ID: creates a new subject")
	fmt.Println(" * delete ID: removes subject")
}

func repoUsage() {
	fmt.Println("Subject repo commands:")
	fmt.Println(" * list: list user repos")
	fmt.Println(" * show ID: show repo ID in details")
	fmt.Println(" * edit ID: update repo ID info, see -h")
	fmt.Println(" * create ID: creates a new repo")
	fmt.Println(" * delete ID: removes repo")
}

func packageUsage() {
	fmt.Println("repo packages commands:")
	fmt.Println(" * list: list repo packages")
	fmt.Println(" * show ID: show package ID in details")
	fmt.Println(" * edit ID: update package ID info, see -h")
	fmt.Println(" * create ID: creates a new package")
	fmt.Println(" * delete ID: removes package")
}

func versionUsage() {
	fmt.Println("Package versions commands:")
	fmt.Println(" * list: list package versions")
	fmt.Println(" * show ID: show package version ID in details with file list")
	fmt.Println(" * edit ID: update package version ID info")
	fmt.Println(" * publish ID: publish package version ID info")
	fmt.Println(" * unpublish ID: unpublish package version ID info")
	fmt.Println(" * create ID: creates a new package version")
	fmt.Println(" * delete ID: removes package version")
	fmt.Println(" * upload PATH [to]: uploads a file")
	fmt.Println(" * download PATH [to]: downloads a file")
	fmt.Println(" * info PATH: file info")
	fmt.Println(" * discard PATH: remove file")
}

func promptConfirm(question string) bool {
	if os.Getenv("GOZ_NONINTERACTIVE") == "1" {
		return true
	}
	fmt.Print(question + "[y/n]:")
	var input string
	fmt.Scanln(&input)
	if input == "y" {
		return true
	}
	return false
}

func main() {

	zerolog.SetGlobalLevel(zerolog.InfoLevel)
	if os.Getenv("GOZ_DEBUG") == "1" {
		zerolog.SetGlobalLevel(zerolog.DebugLevel)
	}

	options := OptionsDef{
		UID:    os.Getenv("GOZ_USER"),
		APIKEY: os.Getenv("GOZ_APIKEY"),
		URL:    os.Getenv("GOZ_URL"),
	}

	// mainOptions := []string{"namespace", "run"}

	var userID string
	var apiKey string
	var password string
	var url string
	var showVersion bool

	flag.StringVar(&userID, "user", "", "login identifier")
	flag.StringVar(&apiKey, "apikey", "", "Authentication API Key")
	flag.StringVar(&password, "password", "", "user password (for *me* access)")
	flag.StringVar(&url, "url", "", "URL to Gozilla host")
	flag.BoolVar(&showVersion, "version", false, "show client version")
	flag.Usage = cliUsage
	flag.Parse()

	if showVersion {
		fmt.Printf("Version: %s\n", Version)
		os.Exit(0)
	}

	if apiKey != "" {
		options.APIKEY = apiKey
	}
	if password != "" {
		options.APIKEY = password
	}
	if url != "" {
		options.URL = url
	}
	if userID != "" {
		options.UID = userID
	}

	if options.UID == "" || options.URL == "" || options.APIKEY == "" {
		fmt.Println("user, apikey and url options must not be empty")
		os.Exit(1)
	}

	args := flag.Args()

	if len(args) == 0 {
		flag.Usage()
		os.Exit(1)
	}

	var err error

	switch args[0] {
	case "me":
		err = handleMe(options)
		break
	case "subject":
		if len(args) == 1 {
			subjectUsage()
			os.Exit(1)
		}
		err = handleSubject(options, args[1:])
		break
	case "repo":
		if len(args) == 1 {
			repoUsage()
			os.Exit(1)
		}
		err = handleRepo(options, args[1:])
		break
	case "package":
		if len(args) == 1 {
			packageUsage()
			os.Exit(1)
		}
		err = handlePackage(options, args[1:])
		break
	case "version":
		if len(args) == 1 {
			versionUsage()
			os.Exit(1)
		}
		err = handlePackageVersion(options, args[1:])
		break
	default:
		elts := strings.Split(args[len(args)-1], "/")
		switch len(elts) {
		case 1:
			err = handleSubject(options, args)
			break
		case 2:
			err = handleRepo(options, args)
			break
		case 3:
			err = handlePackage(options, args)
			break
		case 4:
			err = handlePackageVersion(options, args)
			break
		default:
			err = fmt.Errorf("unknown command: %s", os.Args[1])
		}
	}

	if err != nil {
		fmt.Printf("%s\n", err)
		os.Exit(1)
	}

}

func deleteSubject(options OptionsDef, packageSubject string, packageForce bool) error {
	fmt.Printf("Delete subject %s\n", packageSubject)
	client := &http.Client{}
	req, _ := http.NewRequest("GET", fmt.Sprintf("%s/api/v1.0/%s/%s", options.URL, "repos", packageSubject), nil)
	if options.APIKEY != "" {
		req.Header.Set("X-GOZ-USER", options.UID)
		req.Header.Set("X-GOZ-APIKEY", options.APIKEY)
	}
	resp, err := client.Do(req)
	if err != nil {
		return err
	}

	defer resp.Body.Close()
	if resp.StatusCode != 200 {
		fmt.Printf("Invalid response code: %d\n", resp.StatusCode)
		body, err := ioutil.ReadAll(resp.Body)
		if err == nil {
			fmt.Printf("Error: %s", body)
		}
		return fmt.Errorf("Delete command failed")
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	type PackageShowResult struct {
		Subject string     `json:"subject"`
		Repos   []goz.Repo `json:"repos"`
	}
	var packsResult PackageShowResult
	err = json.Unmarshal(body, &packsResult)
	if err != nil {
		return err
	}

	nbFiles := len(packsResult.Repos)
	if nbFiles > 0 && !packageForce {
		return fmt.Errorf("Subject contains %d repo, cannot remove it or use force option to delete whole content", nbFiles)
	}

	deleteStatus := true
	if nbFiles > 0 && packageForce {
		for _, v := range packsResult.Repos {
			errDelete := deleteRepo(options, v.Subject, v.ID, true)
			if errDelete != nil {
				deleteStatus = false
			}
		}
	}

	if !deleteStatus {
		return fmt.Errorf("Failed to delete all repos")
	}

	req, _ = http.NewRequest("DELETE", fmt.Sprintf("%s/api/v1.0/%s/%s", options.URL, "repos", packageSubject), nil)
	if options.APIKEY != "" {
		req.Header.Set("X-GOZ-USER", options.UID)
		req.Header.Set("X-GOZ-APIKEY", options.APIKEY)
	}
	resp, _ = client.Do(req)
	if err != nil {
		return err
	}
	if resp.StatusCode != 200 {
		body, _ := ioutil.ReadAll(resp.Body)

		return fmt.Errorf("Failed to delete: %s", body)
	}
	fmt.Printf("Subject %s deleted", packageSubject)
	return nil
}

func deleteRepo(options OptionsDef, packageSubject, packageRepo string, packageForce bool) error {
	fmt.Printf("Delete repo %s\n", packageRepo)
	client := &http.Client{}
	req, _ := http.NewRequest("GET", fmt.Sprintf("%s/api/v1.0/%s/%s/%s", options.URL, "repos", packageSubject, packageRepo), nil)
	if options.APIKEY != "" {
		req.Header.Set("X-GOZ-USER", options.UID)
		req.Header.Set("X-GOZ-APIKEY", options.APIKEY)
	}
	resp, err := client.Do(req)
	if err != nil {
		return err
	}

	defer resp.Body.Close()
	if resp.StatusCode != 200 {
		fmt.Printf("Invalid response code: %d\n", resp.StatusCode)
		body, err := ioutil.ReadAll(resp.Body)
		if err == nil {
			fmt.Printf("Error: %s", body)
		}
		return fmt.Errorf("Delete command failed")
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	type PackageShowResult struct {
		Subject  string        `json:"subject"`
		Repo     string        `json:"repo"`
		Packages []goz.Package `json:"packages"`
	}
	var packsResult PackageShowResult
	err = json.Unmarshal(body, &packsResult)
	if err != nil {
		return err
	}

	nbFiles := len(packsResult.Packages)
	if nbFiles > 0 && !packageForce {
		return fmt.Errorf("Repo contains %d packages, cannot remove it or use force option to delete whole content", nbFiles)
	}

	deleteStatus := true
	if nbFiles > 0 && packageForce {
		for _, v := range packsResult.Packages {
			errDelete := deletePackage(options, v.Subject, v.Repo, v.ID, true)
			if errDelete != nil {
				deleteStatus = false
			}
		}
	}

	if !deleteStatus {
		return fmt.Errorf("Failed to delete all packages")
	}

	req, _ = http.NewRequest("DELETE", fmt.Sprintf("%s/api/v1.0/%s/%s/%s", options.URL, "packages", packageSubject, packageRepo), nil)
	if options.APIKEY != "" {
		req.Header.Set("X-GOZ-USER", options.UID)
		req.Header.Set("X-GOZ-APIKEY", options.APIKEY)
	}
	resp, _ = client.Do(req)
	if err != nil {
		return err
	}
	if resp.StatusCode != 200 {
		body, _ := ioutil.ReadAll(resp.Body)

		return fmt.Errorf("Failed to delete: %s", body)
	}
	fmt.Printf("Repo %s deleted", packageRepo)
	return nil
}

func deletePackage(options OptionsDef, packageSubject, packageRepo, packagePackage string, packageForce bool) error {
	fmt.Printf("Delete package %s\n", packagePackage)
	client := &http.Client{}
	req, _ := http.NewRequest("GET", fmt.Sprintf("%s/api/v1.0/%s/%s/%s/%s/versions", options.URL, "packages", packageSubject, packageRepo, packagePackage), nil)
	if options.APIKEY != "" {
		req.Header.Set("X-GOZ-USER", options.UID)
		req.Header.Set("X-GOZ-APIKEY", options.APIKEY)
	}
	resp, err := client.Do(req)
	if err != nil {
		return err
	}

	defer resp.Body.Close()
	if resp.StatusCode != 200 {
		fmt.Printf("Invalid response code: %d\n", resp.StatusCode)
		body, err := ioutil.ReadAll(resp.Body)
		if err == nil {
			fmt.Printf("Error: %s", body)
		}
		return fmt.Errorf("Delete command failed")
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	type PackageShowResult struct {
		Subject  string               `json:"subject"`
		Repo     string               `json:"repo"`
		Package  goz.Package          `json:"package"`
		Versions []goz.PackageVersion `json:"versions"`
	}
	var packsResult PackageShowResult
	err = json.Unmarshal(body, &packsResult)
	if err != nil {
		return err
	}

	nbFiles := len(packsResult.Versions)
	if nbFiles > 0 && !packageForce {
		return fmt.Errorf("Package contains %d versions, cannot remove it or use force option to delete whole content", nbFiles)
	}

	deleteStatus := true
	if nbFiles > 0 && packageForce {
		for _, v := range packsResult.Versions {
			errDelete := deleteVersion(options, v.Subject, v.Repo, v.Package, v.Version, true)
			if errDelete != nil {
				deleteStatus = false
			}
		}
	}

	if !deleteStatus {
		return fmt.Errorf("Failed to delete all package versions")
	}

	req, _ = http.NewRequest("DELETE", fmt.Sprintf("%s/api/v1.0/%s/%s/%s/%s", options.URL, "packages", packageSubject, packageRepo, packagePackage), nil)
	if options.APIKEY != "" {
		req.Header.Set("X-GOZ-USER", options.UID)
		req.Header.Set("X-GOZ-APIKEY", options.APIKEY)
	}
	resp, _ = client.Do(req)
	if err != nil {
		return err
	}
	if resp.StatusCode != 200 {
		body, _ := ioutil.ReadAll(resp.Body)

		return fmt.Errorf("Failed to delete: %s", body)
	}
	fmt.Printf("Package %s deleted", packagePackage)

	return nil
}

func deleteVersion(options OptionsDef, packageSubject, packageRepo, packagePackage, packageVersion string, packageForce bool) error {
	fmt.Printf("Delete package version %s\n", packageVersion)

	client := &http.Client{}
	req, _ := http.NewRequest("GET", fmt.Sprintf("%s/api/v1.0/%s/%s/%s/%s/versions/%s", options.URL, "packages", packageSubject, packageRepo, packagePackage, packageVersion), nil)
	if options.APIKEY != "" {
		req.Header.Set("X-GOZ-USER", options.UID)
		req.Header.Set("X-GOZ-APIKEY", options.APIKEY)
	}
	resp, err := client.Do(req)
	if err != nil {
		return err
	}

	defer resp.Body.Close()
	if resp.StatusCode != 200 {
		fmt.Printf("Invalid response code: %d\n", resp.StatusCode)
		body, err := ioutil.ReadAll(resp.Body)
		if err == nil {
			fmt.Printf("Error: %s", body)
		}
		return fmt.Errorf("Delete command failed")
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	type PackageShowResult struct {
		Subject string              `json:"subject"`
		Repo    string              `json:"repo"`
		Package goz.Package         `json:"package"`
		Version goz.PackageVersion  `json:"version"`
		Files   []goz.RawFileObject `json:"files"`
	}
	var packsResult PackageShowResult
	err = json.Unmarshal(body, &packsResult)
	if err != nil {
		return err
	}

	nbFiles := len(packsResult.Files)
	if nbFiles > 0 && !packageForce {
		return fmt.Errorf("Package version contains %d files, cannot remove it or use force option to delete whole content", nbFiles)
	}

	deleteStatus := true
	if nbFiles > 0 && packageForce {
		gozCfg, gozErr := getConfig(options)
		if gozErr != nil {
			return fmt.Errorf("Failed to fetch configuration from remote server")
		}
		storeFileHandler := NewStorageFileHandler(Config{
			StorageType: gozCfg.Storage,
			GozServer:   options.URL,
			UID:         options.UID,
			APIKEY:      options.APIKEY,
			APIVersion:  gozCfg.APIVersion,

			Subject: packageSubject,
			Repo:    packageRepo,
			Package: packagePackage,
			Version: packageVersion,
		}, packageSubject)
		for _, file := range packsResult.Files {
			fmt.Printf("Deleting %s\n", file.GetName())

			if storeFileHandler == nil {
				return fmt.Errorf("Failed to init storage")
			}
			err := storeFileHandler.DeleteFile(file.GetName())
			if err != nil {
				deleteStatus = false
				fmt.Printf("Failed to delete %s: %s\n", file.GetPath(), err)
				continue
			}
		}
	}

	if !deleteStatus {
		return fmt.Errorf("Failed to delete all files")
	}

	req, _ = http.NewRequest("DELETE", fmt.Sprintf("%s/api/v1.0/%s/%s/%s/%s/versions/%s", options.URL, "packages", packageSubject, packageRepo, packagePackage, packageVersion), nil)
	if options.APIKEY != "" {
		req.Header.Set("X-GOZ-USER", options.UID)
		req.Header.Set("X-GOZ-APIKEY", options.APIKEY)
	}
	resp, _ = client.Do(req)
	if err != nil {
		return err
	}
	if resp.StatusCode != 200 {
		body, _ := ioutil.ReadAll(resp.Body)

		return fmt.Errorf("Failed to delete: %s", body)
	}
	fmt.Printf("Package version %s deleted", packageVersion)

	return nil
}
